// xkas-plus v14+1
arch 6502
header
banksize $4000

org $00

print "current pc: ",org

define exit      $eaec // cpu address where the original routine left off. euivalent to 0x7eafc in rom
define freeram   $0110 // free address in ram 
define dtetbl    $8000 // cpu address where the dte table is stored
define freespace $f84a // space to place the dte routine in
define txt_ptr   $3e   // pointer to current tile in memory
define txt_ptr2  $3f

org $7eaf6 {}       // location where the original text loading routine is. euivalent to $eae6 in cpu
  ldy #$00        // part of the original ff2's routine
  jmp {freespace} // move pc to free space


org $7f85a        // equivalent to $f84a in ram

print "current pc: ",org

entrypoint: 
  beq terminator  // part of the original routine
  lsr $0110       // Load the first/second flag into the carry
  lda ({txt_ptr}),y // Load the next character 
  bcs secondtime  // If the second flag is set branch  
  cmp #$3b
  bcc abort
  cmp #$6d        // Check that the character is in the range [0x3b-0x6d)
  bcs abort



  inc {freeram}  // Set the first/second toggle to second

   sbc #($3c-1)  // a is the new character, so subtract $3c to get the correct offset
//sec - carry is clear so we can skip sec by subtracting 1 to the value to subtract
  asl
  tay

  lda $8000,y  

  
abort: 
  jmp {exit}


secondtime: 
  ldy {txt_ptr}  // first/second toggle is cleared already so don't need to set it
  bne skip

  dec {txt_ptr2} // decrement the pointer
  
skip: 
  dec {txt_ptr}
  sbc #$3c       // a is the new character, so subtract $3c to get the correct offset
//sec - carry is already set from the `lsr $110` so we don't need to set it
  asl
  tay
  lda {dtetbl}+1,y
  jmp {exit}


terminator:     // handles end string
  jmp $f7dd
