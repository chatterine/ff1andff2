#!/bin/bash

# Extract text from the English Prototype using abcde's cartographer module

printf "Final Fantasy 2 Text Dumper and Auto Formatter\n"
sleep 0.25
printf "Dumping text:\n"
sleep 0.25

perl tool/abcde/abcde.pl -s -cm Cartographer roms/ff2_rom/proto/ff2_proto.nes asm/cartographer/ff2_proto_commands.txt txt/ff2_txt/prototype/abcde_dump/ff2_script -m

# Convert castlefynn script dumps into Atlas

printf "Converting CBT's Castlefynn script dumps into Atlas:\n"
sleep 0.25

# CBT

python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/cbt/fynn/data_enemy.txt atlas ff2_cbt_script_000.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/cbt/fynn/data_attack.txt atlas ff2_cbt_script_001.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/cbt/fynn/data_btl.txt atlas ff2_cbt_script_002.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/cbt/fynn/data_bank1.txt atlas ff2_cbt_script_003.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/cbt/fynn/data_bank2.txt atlas ff2_cbt_script_004.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/cbt/fynn/data_misc1.txt atlas ff2_cbt_script_005.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/cbt/fynn/data_misc2.txt atlas ff2_cbt_script_006.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/cbt/fynn/data_misc2.txt atlas ff2_cbt_script_007.txt --include_end_tags

mv export/*.txt txt/ff2_txt/cbt/atlas/

# German

printf "Converting German Castlefynn script dumps into Atlas:\n"
sleep 0.25

python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/de/fynn/data_enemy.txt atlas ff2_de_script_000.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/de/fynn/data_attack.txt atlas ff2_de_script_001.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/de/fynn/data_btl.txt atlas ff2_de_script_002.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/de/fynn/data_bank1.txt atlas ff2_de_script_003.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/de/fynn/data_bank2.txt atlas ff2_de_script_004.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/de/fynn/data_misc1.txt atlas ff2_de_script_005.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/de/fynn/data_misc2.txt atlas ff2_de_script_006.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/de/fynn/data_misc2.txt atlas ff2_de_script_007.txt --include_end_tags

mv export/*.txt txt/ff2_txt/de/atlas/

# Neo Demiforce

printf "Converting Neo Demiforce's Castlefynn script dumps into Atlas:\n"
sleep 0.25

python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/demiforce/fynn/data_enemy.txt atlas ff2_demiforce_script_000.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/demiforce/fynn/data_attack.txt atlas ff2_demiforce_script_001.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/demiforce/fynn/data_btl.txt atlas ff2_demiforce_script_002.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/demiforce/fynn/data_bank1.txt atlas ff2_demiforce_script_003.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/demiforce/fynn/data_bank2.txt atlas ff2_demiforce_script_004.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/demiforce/fynn/data_misc1.txt atlas ff2_demiforce_script_005.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/demiforce/fynn/data_misc2.txt atlas ff2_demiforce_script_006.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/demiforce/fynn/data_misc2.txt atlas ff2_demiforce_script_007.txt --include_end_tags

mv export/*.txt txt/ff2_txt/demiforce/atlas/

# Spanish

printf "Converting Spanish Castlefynn script dumps into Atlas:\n"
sleep 0.25

python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/es/fynn/data_enemy.txt atlas ff2_es_script_000.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/es/fynn/data_attack.txt atlas ff2_es_script_001.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/es/fynn/data_btl.txt atlas ff2_es_script_002.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/es/fynn/data_bank1.txt atlas ff2_es_script_003.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/es/fynn/data_bank2.txt atlas ff2_es_script_004.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/es/fynn/data_misc1.txt atlas ff2_es_script_005.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/es/fynn/data_misc2.txt atlas ff2_es_script_006.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/es/fynn/data_misc2.txt atlas ff2_es_script_007.txt --include_end_tags

mv export/*.txt txt/ff2_txt/es/atlas/

# French

printf "Converting French Castlefynn script dumps into Atlas:\n"
sleep 0.25

python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/fr/fynn/data_enemy.txt atlas ff2_fr_script_000.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/fr/fynn/data_attack.txt atlas ff2_fr_script_001.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/fr/fynn/data_btl.txt atlas ff2_fr_script_002.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/fr/fynn/data_bank1.txt atlas ff2_fr_script_003.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/fr/fynn/data_bank2.txt atlas ff2_fr_script_004.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/fr/fynn/data_misc1.txt atlas ff2_fr_script_005.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/fr/fynn/data_misc2.txt atlas ff2_fr_script_006.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/fr/fynn/data_misc2.txt atlas ff2_fr_script_007.txt --include_end_tags

mv export/*.txt txt/ff2_txt/fr/atlas/

# Italian

printf "Converting Italian Castlefynn script dumps into Atlas:\n"
sleep 0.25

python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/it/fynn/data_enemy.txt atlas ff2_it_script_000.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/it/fynn/data_attack.txt atlas ff2_it_script_001.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/it/fynn/data_btl.txt atlas ff2_it_script_002.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/it/fynn/data_bank1.txt atlas ff2_it_script_003.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/it/fynn/data_bank2.txt atlas ff2_it_script_004.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/it/fynn/data_misc1.txt atlas ff2_it_script_005.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/it/fynn/data_misc2.txt atlas ff2_it_script_006.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/it/fynn/data_misc2.txt atlas ff2_it_script_007.txt --include_end_tags

mv export/*.txt txt/ff2_txt/it/atlas/

# Japanese

printf "Converting Japanese Castlefynn script dumps into Atlas:\n"
sleep 0.25

python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/jp/fynn/data_enemy.txt atlas ff2_jp_script_000.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/jp/fynn/data_attack.txt atlas ff2_jp_script_001.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/jp/fynn/data_btl.txt atlas ff2_jp_script_002.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/jp/fynn/data_bank1.txt atlas ff2_jp_script_003.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/jp/fynn/data_bank2.txt atlas ff2_jp_script_004.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/jp/fynn/data_misc1.txt atlas ff2_jp_script_005.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/jp/fynn/data_misc2.txt atlas ff2_jp_script_006.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/jp/fynn/data_misc2.txt atlas ff2_jp_script_007.txt --include_end_tags

mv export/*.txt txt/ff2_txt/jp/atlas/

# Refurbished

printf "Converting Refurbished Castlefynn script dumps into Atlas:\n"
sleep 0.25

python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/refurbished/fynn/data_enemy.txt atlas ff2_refurbished_script_000.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/refurbished/fynn/data_attack.txt atlas ff2_refurbished_script_001.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/refurbished/fynn/data_btl.txt atlas ff2_refurbished_script_002.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/refurbished/fynn/data_bank1.txt atlas ff2_refurbished_script_003.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/refurbished/fynn/data_bank2.txt atlas ff2_refurbished_script_004.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/refurbished/fynn/data_misc1.txt atlas ff2_refurbished_script_005.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/refurbished/fynn/data_misc2.txt atlas ff2_refurbished_script_006.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/refurbished/fynn/data_misc2.txt atlas ff2_refurbished_script_007.txt --include_end_tags

mv export/*.txt txt/ff2_txt/refurbished/atlas/

# Russian

printf "Converting Russian Castlefynn script dumps into Atlas:\n"
sleep 0.25

python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/ru/fynn/data_enemy.txt atlas ff2_ru_script_000.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/ru/fynn/data_attack.txt atlas ff2_ru_script_001.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/ru/fynn/data_btl.txt atlas ff2_ru_script_002.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/ru/fynn/data_bank1.txt atlas ff2_ru_script_003.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/ru/fynn/data_bank2.txt atlas ff2_ru_script_004.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/ru/fynn/data_misc1.txt atlas ff2_ru_script_005.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/ru/fynn/data_misc2.txt atlas ff2_ru_script_006.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/ru/fynn/data_misc2.txt atlas ff2_ru_script_007.txt --include_end_tags

mv export/*.txt txt/ff2_txt/ru/atlas/

# Swedish

printf "Converting Swedish Castlefynn script dumps into Atlas:\n"
sleep 0.25

python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/sv/fynn/data_enemy.txt atlas ff2_sv_script_000.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/sv/fynn/data_attack.txt atlas ff2_sv_script_001.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/sv/fynn/data_btl.txt atlas ff2_sv_script_002.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/sv/fynn/data_bank1.txt atlas ff2_sv_script_003.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/sv/fynn/data_bank2.txt atlas ff2_sv_script_004.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/sv/fynn/data_misc1.txt atlas ff2_sv_script_005.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/sv/fynn/data_misc2.txt atlas ff2_sv_script_006.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/sv/fynn/data_misc2.txt atlas ff2_sv_script_007.txt --include_end_tags

mv export/*.txt txt/ff2_txt/sv/atlas/

# Toma

printf "Converting Toma's' Castlefynn script dumps into Atlas:\n"
sleep 0.25

python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/toma/fynn/data_enemy.txt atlas ff2_toma_script_000.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/toma/fynn/data_attack.txt atlas ff2_toma_script_001.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/toma/fynn/data_btl.txt atlas ff2_toma_script_002.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/toma/fynn/data_bank1.txt atlas ff2_toma_script_003.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/toma/fynn/data_bank2.txt atlas ff2_toma_script_004.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/toma/fynn/data_misc1.txt atlas ff2_toma_script_005.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/toma/fynn/data_misc2.txt atlas ff2_toma_script_006.txt --include_end_tags
python3 tool/python-fynn2atlas-main/fynn2atlas.py txt/ff2_txt/toma/fynn/data_misc2.txt atlas ff2_toma_script_007.txt --include_end_tags

mv export/*.txt txt/ff2_txt/toma/atlas/

# Conversion to JSON

printf "Converting CBT Atlas script files to JSON\n"
sleep 0.25

python3 tool/json-convert/converter.py t2j txt/ff2_txt/cbt/atlas/ff2_cbt_script_000.txt txt/ff2_txt/cbt/json/ff2_cbt_script_000.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/cbt/atlas/ff2_cbt_script_001.txt txt/ff2_txt/cbt/json/ff2_cbt_script_001.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/cbt/atlas/ff2_cbt_script_002.txt txt/ff2_txt/cbt/json/ff2_cbt_script_002.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/cbt/atlas/ff2_cbt_script_003.txt txt/ff2_txt/cbt/json/ff2_cbt_script_003.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/cbt/atlas/ff2_cbt_script_004.txt txt/ff2_txt/cbt/json/ff2_cbt_script_004.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/cbt/atlas/ff2_cbt_script_005.txt txt/ff2_txt/cbt/json/ff2_cbt_script_005.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/cbt/atlas/ff2_cbt_script_006.txt txt/ff2_txt/cbt/json/ff2_cbt_script_006.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/cbt/atlas/ff2_cbt_script_007.txt txt/ff2_txt/cbt/json/ff2_cbt_script_007.json

printf "Converting German Atlas script files to JSON\n"
sleep 0.25

python3 tool/json-convert/converter.py t2j txt/ff2_txt/de/atlas/ff2_de_script_000.txt txt/ff2_txt/de/json/ff2_de_script_000.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/de/atlas/ff2_de_script_001.txt txt/ff2_txt/de/json/ff2_de_script_001.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/de/atlas/ff2_de_script_002.txt txt/ff2_txt/de/json/ff2_de_script_002.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/de/atlas/ff2_de_script_003.txt txt/ff2_txt/de/json/ff2_de_script_003.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/de/atlas/ff2_de_script_004.txt txt/ff2_txt/de/json/ff2_de_script_004.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/de/atlas/ff2_de_script_005.txt txt/ff2_txt/de/json/ff2_de_script_005.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/de/atlas/ff2_de_script_006.txt txt/ff2_txt/de/json/ff2_de_script_006.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/de/atlas/ff2_de_script_007.txt txt/ff2_txt/de/json/ff2_de_script_007.json

printf "Converting Spanish Atlas script files to JSON\n"
sleep 0.25

python3 tool/json-convert/converter.py t2j txt/ff2_txt/es/atlas/ff2_es_script_000.txt txt/ff2_txt/es/json/ff2_es_script_000.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/es/atlas/ff2_es_script_001.txt txt/ff2_txt/es/json/ff2_es_script_001.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/es/atlas/ff2_es_script_002.txt txt/ff2_txt/es/json/ff2_es_script_002.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/es/atlas/ff2_es_script_003.txt txt/ff2_txt/es/json/ff2_es_script_003.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/es/atlas/ff2_es_script_004.txt txt/ff2_txt/es/json/ff2_es_script_004.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/es/atlas/ff2_es_script_005.txt txt/ff2_txt/es/json/ff2_es_script_005.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/es/atlas/ff2_es_script_006.txt txt/ff2_txt/es/json/ff2_es_script_006.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/es/atlas/ff2_es_script_007.txt txt/ff2_txt/es/json/ff2_es_script_007.json

printf "Converting French Atlas script files to JSON\n"
sleep 0.25

python3 tool/json-convert/converter.py t2j txt/ff2_txt/fr/atlas/ff2_fr_script_000.txt txt/ff2_txt/fr/json/ff2_fr_script_000.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/fr/atlas/ff2_fr_script_001.txt txt/ff2_txt/fr/json/ff2_fr_script_001.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/fr/atlas/ff2_fr_script_002.txt txt/ff2_txt/fr/json/ff2_fr_script_002.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/fr/atlas/ff2_fr_script_003.txt txt/ff2_txt/fr/json/ff2_fr_script_003.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/fr/atlas/ff2_fr_script_004.txt txt/ff2_txt/fr/json/ff2_fr_script_004.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/fr/atlas/ff2_fr_script_005.txt txt/ff2_txt/fr/json/ff2_fr_script_005.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/fr/atlas/ff2_fr_script_006.txt txt/ff2_txt/fr/json/ff2_fr_script_006.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/fr/atlas/ff2_fr_script_007.txt txt/ff2_txt/fr/json/ff2_fr_script_007.json

printf "Converting Italian Atlas script files to JSON\n"
sleep 0.25

python3 tool/json-convert/converter.py t2j txt/ff2_txt/it/atlas/ff2_it_script_000.txt txt/ff2_txt/it/json/ff2_it_script_000.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/it/atlas/ff2_it_script_001.txt txt/ff2_txt/it/json/ff2_it_script_001.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/it/atlas/ff2_it_script_002.txt txt/ff2_txt/it/json/ff2_it_script_002.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/it/atlas/ff2_it_script_003.txt txt/ff2_txt/it/json/ff2_it_script_003.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/it/atlas/ff2_it_script_004.txt txt/ff2_txt/it/json/ff2_it_script_004.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/it/atlas/ff2_it_script_005.txt txt/ff2_txt/it/json/ff2_it_script_005.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/it/atlas/ff2_it_script_006.txt txt/ff2_txt/it/json/ff2_it_script_006.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/it/atlas/ff2_it_script_007.txt txt/ff2_txt/it/json/ff2_it_script_007.json

printf "Converting Japanese Atlas script files to JSON\n"
sleep 0.25

python3 tool/json-convert/converter.py t2j txt/ff2_txt/jp/atlas/ff2_jp_script_000.txt txt/ff2_txt/jp/json/ff2_jp_script_000.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/jp/atlas/ff2_jp_script_001.txt txt/ff2_txt/jp/json/ff2_jp_script_001.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/jp/atlas/ff2_jp_script_002.txt txt/ff2_txt/jp/json/ff2_jp_script_002.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/jp/atlas/ff2_jp_script_003.txt txt/ff2_txt/jp/json/ff2_jp_script_003.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/jp/atlas/ff2_jp_script_004.txt txt/ff2_txt/jp/json/ff2_jp_script_004.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/jp/atlas/ff2_jp_script_005.txt txt/ff2_txt/jp/json/ff2_jp_script_005.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/jp/atlas/ff2_jp_script_006.txt txt/ff2_txt/jp/json/ff2_jp_script_006.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/jp/atlas/ff2_jp_script_007.txt txt/ff2_txt/jp/json/ff2_jp_script_007.json

printf "Converting Prototype Atlas script files to JSON\n"
sleep 0.25

python3 tool/json-convert/converter.py t2j txt/ff2_txt/prototype/cartographer_dump/ff2_script_000.txt txt/ff2_txt/prototype/json/ff2_script_000.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/prototype/cartographer_dump/ff2_script_001.txt txt/ff2_txt/prototype/json/ff2_script_001.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/prototype/cartographer_dump/ff2_script_002.txt txt/ff2_txt/prototype/json/ff2_script_002.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/prototype/cartographer_dump/ff2_script_003.txt txt/ff2_txt/prototype/json/ff2_script_003.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/prototype/cartographer_dump/ff2_script_004.txt txt/ff2_txt/prototype/json/ff2_script_004.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/prototype/cartographer_dump/ff2_script_005.txt txt/ff2_txt/prototype/json/ff2_script_005.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/prototype/cartographer_dump/ff2_script_006.txt txt/ff2_txt/prototype/json/ff2_script_006.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/prototype/cartographer_dump/ff2_script_007.txt txt/ff2_txt/prototype/json/ff2_script_007.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/prototype/cartographer_dump/ff2_script_008.txt txt/ff2_txt/prototype/json/ff2_script_008.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/prototype/cartographer_dump/ff2_script_009.txt txt/ff2_txt/prototype/json/ff2_script_009.json

printf "Converting Refurbished Atlas script files to JSON\n"
sleep 0.25

python3 tool/json-convert/converter.py t2j txt/ff2_txt/refurbished/atlas/ff2_refurbished_script_000.txt txt/ff2_txt/refurbished/json/ff2_refurbished_script_000.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/refurbished/atlas/ff2_refurbished_script_001.txt txt/ff2_txt/refurbished/json/ff2_refurbished_script_001.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/refurbished/atlas/ff2_refurbished_script_002.txt txt/ff2_txt/refurbished/json/ff2_refurbished_script_002.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/refurbished/atlas/ff2_refurbished_script_003.txt txt/ff2_txt/refurbished/json/ff2_refurbished_script_003.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/refurbished/atlas/ff2_refurbished_script_004.txt txt/ff2_txt/refurbished/json/ff2_refurbished_script_004.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/refurbished/atlas/ff2_refurbished_script_005.txt txt/ff2_txt/refurbished/json/ff2_refurbished_script_005.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/refurbished/atlas/ff2_refurbished_script_006.txt txt/ff2_txt/refurbished/json/ff2_refurbished_script_006.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/refurbished/atlas/ff2_refurbished_script_007.txt txt/ff2_txt/refurbished/json/ff2_refurbished_script_007.json

printf "Converting Russian Atlas script files to JSON\n"
sleep 0.25

python3 tool/json-convert/converter.py t2j txt/ff2_txt/ru/atlas/ff2_ru_script_000.txt txt/ff2_txt/ru/json/ff2_ru_script_000.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/ru/atlas/ff2_ru_script_001.txt txt/ff2_txt/ru/json/ff2_ru_script_001.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/ru/atlas/ff2_ru_script_002.txt txt/ff2_txt/ru/json/ff2_ru_script_002.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/ru/atlas/ff2_ru_script_003.txt txt/ff2_txt/ru/json/ff2_ru_script_003.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/ru/atlas/ff2_ru_script_004.txt txt/ff2_txt/ru/json/ff2_ru_script_004.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/ru/atlas/ff2_ru_script_005.txt txt/ff2_txt/ru/json/ff2_ru_script_005.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/ru/atlas/ff2_ru_script_006.txt txt/ff2_txt/ru/json/ff2_ru_script_006.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/ru/atlas/ff2_ru_script_007.txt txt/ff2_txt/ru/json/ff2_ru_script_007.json

printf "Converting Swedish Atlas script files to JSON\n"
sleep 0.25

python3 tool/json-convert/converter.py t2j txt/ff2_txt/sv/atlas/ff2_sv_script_000.txt txt/ff2_txt/sv/json/ff2_sv_script_000.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/sv/atlas/ff2_sv_script_001.txt txt/ff2_txt/sv/json/ff2_sv_script_001.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/sv/atlas/ff2_sv_script_002.txt txt/ff2_txt/sv/json/ff2_sv_script_002.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/sv/atlas/ff2_sv_script_003.txt txt/ff2_txt/sv/json/ff2_sv_script_003.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/sv/atlas/ff2_sv_script_004.txt txt/ff2_txt/sv/json/ff2_sv_script_004.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/sv/atlas/ff2_sv_script_005.txt txt/ff2_txt/sv/json/ff2_sv_script_005.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/sv/atlas/ff2_sv_script_006.txt txt/ff2_txt/sv/json/ff2_sv_script_006.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/sv/atlas/ff2_sv_script_007.txt txt/ff2_txt/sv/json/ff2_sv_script_007.json

printf "Converting Toma's Atlas script files to JSON\n"
sleep 0.25

python3 tool/json-convert/converter.py t2j txt/ff2_txt/toma/atlas/ff2_toma_script_000.txt txt/ff2_txt/toma/json/ff2_toma_script_000.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/toma/atlas/ff2_toma_script_001.txt txt/ff2_txt/toma/json/ff2_toma_script_001.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/toma/atlas/ff2_toma_script_002.txt txt/ff2_txt/toma/json/ff2_toma_script_002.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/toma/atlas/ff2_toma_script_003.txt txt/ff2_txt/toma/json/ff2_toma_script_003.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/toma/atlas/ff2_toma_script_004.txt txt/ff2_txt/toma/json/ff2_toma_script_004.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/toma/atlas/ff2_toma_script_005.txt txt/ff2_txt/toma/json/ff2_toma_script_005.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/toma/atlas/ff2_toma_script_006.txt txt/ff2_txt/toma/json/ff2_toma_script_006.json
python3 tool/json-convert/converter.py t2j txt/ff2_txt/toma/atlas/ff2_toma_script_007.txt txt/ff2_txt/toma/json/ff2_toma_script_007.json

printf "All finished."
