# List of Tools

This is general info on all tools that ship with this.

## Executables

### xkas plus v14+1

A binary version of a fork of Near's xkas assembler created by devinacker. Download the source code for it version [here](https://github.com/devinacker/xkas-plus) or get the Windows version (source included) [here](https://github.com/devinacker/xkas-plus/releases/tag/v14%2B1).

**Usage**: `./xkas file.asm <file.nes>`

## Python scripts

### json-convert/converter.py

A python script created by Breno (with some tweaks made by Chatgpt, matsu and me) to convert script files generated with Cartographer into a format compatible with Crowdin/Weblate and convert Crowdin or Weblate JSON files into an Atlas-compatible format.

### python-fynn2atlas.py

A tool to convert Castlefynn script dumps into Atlas ones and vice versa created by June. Read its own README.md file to learn to use it.
