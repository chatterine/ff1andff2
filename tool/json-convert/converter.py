import argparse
import json

def convert_to_json(txtfile, jsonfile):
    with open(txtfile, 'r', encoding='utf-8') as txt_f:
        lines = [line for line in txt_f] 
    
    comments = []
    instructions = []
    strings = []
    current_string = []
    pointer_count = 1
    
    for line in lines:
        if line.startswith('//POINTER'):
            comments.append(line)
        elif line.startswith('#W16'):
            instructions.append(line)
        elif line.startswith('//GAME') or line.startswith('//BLOCK'):
            continue
        else:
            if '<END>' in line:
                current_string.append(line.replace('<END>', '').rstrip())
                strings.append(''.join(current_string).rstrip() + '<END>')
                current_string = []
            else:
                current_string.append(line.rstrip())

    while len(comments) < len(strings):
        comments.append(f'//POINTER {pointer_count}\n')
        pointer_count += 1
    
    json_data = {}
    for (comment, instruction, string) in zip(comments, instructions, strings):
        json_data[f'{comment.strip()};{instruction.strip()}'] = string
            
    with open(jsonfile, 'w', encoding='utf-8') as json_f:
        json.dump(json_data, json_f, indent=4, ensure_ascii=False)

def convert_to_txt(jsonfile, txtfile):
    with open(jsonfile, 'r', encoding='utf-8') as json_f:
        json_data = json.load(json_f)
        
    comments_instructions = []
    strings = []
        
    for comment_instruction, string in json_data.items():
        comments_instructions.append(comment_instruction)
        strings.append(string)
        
    comments = []
    instructions = []
        
    for comment_instruction in comments_instructions:
        comment, instruction = comment_instruction.split(';')
        comments.append(comment)
        instructions.append(instruction)
        
    with open(txtfile, 'w', encoding='utf-8') as txt_f:
        for (comment, instruction, string) in zip(comments, instructions, strings):
            txt_f.write(f'{comment}\n\n{instruction}\n')
            txt_f.write(f'{string.replace("<LINE>", "<LINE>")}\n\n')

def main():
    parser = argparse.ArgumentParser(description='Bidirectional Atlas <-> JSON converter by Breno')
    parser.add_argument('conversion_type', choices=['t2j', 'j2t'], help='Type of conversion: t2j for TXT to JSON, j2t for JSON to TXT')
    parser.add_argument('input_file', help='Input file name')
    parser.add_argument('output_file', help='Output file name')
    args = parser.parse_args()

    if args.conversion_type == 't2j':
        convert_to_json(args.input_file, args.output_file)
        print('JSON file created!')
    elif args.conversion_type == 'j2t':
        convert_to_txt(args.input_file, args.output_file)
        print('TXT file created!')

if __name__ == '__main__':
    main()
