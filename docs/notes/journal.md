--- Think about this again / explore this idea further later: --- 

- DTE/Diacritics/Both: I probably should hack the game to make it use DTE for the text values associated with (han)dakuten. Or maybe hack the dakuten and handakuten routines to enable diacritics. Or BOTH.
- Dump the script and then upload it to Crowdin: Currently considering this. Breno is currently working on converting my .txt files into .json's
- Change to armips instead of Atlas: i don't really see the benefits to making this change. This Breno guy suggested that I do it though, i gotta ask him why someday
- Set up a consistent schedule for romhacking: Currently doing it solely on weekends.
- Look into this, whatever this is: https://discord.com/channels/266412086291070988/579808915340918786/1234301776463921234
- Learn to debug code.
- Learn Clang/python.
- Learn 6502 asm macros.
- Find out more about ghidra and other static disassemblers. More info here: https://discord.com/channels/266412086291070988/267170507705679872/1234886422645702696
- Learn kana (preferrably with Anki).

--- To-do list ---

1. Immediate Priorities:

- Extract all castlefynn script files from the other translations and adapt them for crowdin
- Put your script files on crowdin using breno's conversion tool
- Make a github repo
- Insert code with an assembler, taking into consideration the bank orgs
- Learn to become a better translator
- Compile a build of mesen with the .net runtime built-in
- Reactivate your forum unificado account

2. Backburner:

- Learn to use makefile
- Finish kuroi's dumper/inserter tutorial
- Look into a way of integrating Crowdin into your workflow by making a help post in https://community.crowdin.com/ this should help :O


[DONE] - Fix your dte code: https://discord.com/channels/266412086291070988/267170507705679872/1235375706628952074

--- Roadmap / checklist ---

- write a dte compression routine.
- dump the japanese ff2 text from ff1+2
- insert a latin font into the game.
- insert some portuguese text into the game.
- setup the crowdin api
- expand/shrink windows.
- reorder some options
- Reverse engineer the hacking in each of the snapshots you got.
- proofread the script.
- do testing.

--- New Credits to Give ---

- jroweboy: Dte routine hack
- Breno: txt to crowdin converter
- byuu and oneup40: xkas assembler (https://github.com/oneup40/xkas-v6-linux)

27/04/2024

A'ight, let's get this started. I'd like to start working on final fantasy 2, which is one of my oldest translation projects (it dates from 2021-2023 iirc) and i never really finished it due to lack of hacking support, so i thought i might as well do the hacking myself. :p Not really sure what to do for now or where to begin though.

I wrote a roadmap as to what I should do thus far (up there).

Note that i'm currently on Linux, so I hopefully won't have much trouble using the pre-existing tools on that platform.

SO I just downloaded the rom and have opened my debugger.

28/04/2024

I just opened up the game, opened the ff2 half, named my characters and brought up the first battle. I open the Tilemap Viewer and hover over the first tile in the starting battle's enemies (they're named Kurokishi). The "ku" tile (#$91) gets stored in the tilemap addr $22a1. I right-click the ku tile and then create a write-only breakpoint with the condition "Value == $91".

First I'll setup the keybindings. As snarf told me:

step into will follow a jsr.
step over will run the routine all iln one go.
step out will run until an rts.
step back will probably undo the last run instruction.

it broke here:

                   $93a5:
93A5 [713A5]  B1 6C          lda ($6c),y [$7608] = $91
93A7 [713A7]  8D 07 20       sta PpuData_2007

So now I break on $91 writes to $7608.

broke here:

                   $9647:
9647 [71647]  91 6C          sta ($6c),y [$7608] = $FF
9649 [71649]  E8             inx

stepping back, i got here:

                   $95f8:
95F8 [715F8]  BD 47 7D       lda $7d47,x

X is zero, so I set a breakpoint on $91 writes to $7d47, which leads me here:

                   $fdb4:
FDB4 [7FDB4]  B1 64          lda ($64),y
FDB6 [7FDB6]  9D 47 7D       sta $7d47,x

($64), y is $afb7, so i check out that address on the copu:

AFB7 [56FB7]               --------data--------
AFB7 [56FB7]  00 00 00 00 00 .db $91 $B4 $90 $95 $00

looks like $56FB7 is my prg-rom address (or $56FC7, if i were to look at the file on a hex editor (this is because nes files have a 10 byte header at the start).

time for a static analysis approach. ROM $56fb7 is cpu $afb7, so I search for "b7 af" (low and high bytes reversed) in the rom and find a match in PRG-ROM $56CF8. That's our pointer table.

Before we dump the script we need a table. Opening the ppu viewer tells us that the $91 byte corresponds directly to the $91 tile. This should tell us all of the non-dakuten and non-handakuten kana.

"Death Rider" (デスライダー) is stored right after "Kurokishi", and notice how Desu Raidaa has diacritics. The bytes for it are 5D D6 F0 CB 5A C2, with 5d and 5a being the "de" and the "da". This means that there's another range for diacritic-enabled characters.

Time to work on the table...

Ok, I got the table setup, but it lacks characters with dakuten and handakuten marks, plus control codes.

To find the control codes I'll look for the first text in the game (which contains たすかりますか ) and then I'll attempt to manually input text there through trial and error. I find the text at $748c1

29/04/2024

I completed the table, sans dictionary entries. Breno over at the saty's discord offered to convert my atlas/cartographer files into a format useable by Crowdin.

30/04/2024

Not much luck thus far. I discussed the possibility of learning new stuff on the go, and updated my "consider this in the future" list.

01/04/2024

First day of May, yay. I recently added a /docs/ folder to my project where I'll be dumping relevant stuff to my project, such as Kingmike's dte tutorial. I've been looking at that one, but I don't fully understand it.

The "tasukarimasuka" text I located earlier (beginning at rom addr $748c1) has its bank loaded to memory, and in the CPU its address is $88c1. I set a read breakpoint on it and get here:

EAE8 [7EAE8]  B1 3E          lda ($3e),y [$88c1] = $18

Also im having to manually type info on the cpu addr's referenced by indirect addr'ing and the current value of A, which is a pita <_< imma have to find a way to copy text on mesen and keep this

On an unrelated side note, I found free space at addr $7f84a (rom)/$f84a (cpu)

So i'll replace the instruction at $eae8 with a jmp to and from free space.

                   $eae6:
EAE6 [7EAE6]  A0 00          ldy #$00
EAE8 [7EAE8]  4C 4A F8       jmp $f84a

                   $f84a:
F84A [7F84A]  B1 3E          lda ($3e),y
F84C [7F84C]  F0 8F          beq $f7dd
F84E [7F84E]  C9 5A          cmp #$5a
F850 [7F850]  D0 02          bne $f854
F852 [7F852]  A9 80          lda #$80
                   $f854:
F854 [7F854]  4C EC EA       jmp $eaec

                   $f7dd:
F7DD [7F7DD]  4C DD EA       jmp $eadd

ok imma start writing the dte code!!!

i'll use the free space from ram addr $110 onwards

from jroweboy on rhdn discord:

"Okay so I'm on mobile so my typing is slow. But here's a quick summary
http://www.6502.org/tutorials/compare_instructions.html
All branch instructions work exclusively on the state of the flags
Bpl and BMI work with the N flag (if n is set it indicates "negative")
cmp works by doing a subtraction between the value and the current accumulator  A
Which ends up setting the n, z, and c flags

So when doing cmp $7f they are checking A - $7f and branching if the value in a is >= $7f

Which is very strange and that's what I'm scratching my head about. Why do they want to include $7f in the value to branch"


my incomplete code:

.org $F84A

define freerambyte = $0110
define dtetable    = $8000
define dte_backup  = $0112

  lda $0110
  cmp #$00
  bne secondtime
  ldy #$00
  lda ($3e),y
  cmp #$3b
  bpl nextcheck

 abort:
  lda ($3e),y
  jmp $eaec

 nextcheck:
  lda ($3e),y
  cmp #$6d
  bpl abort

  inc $0110
  tax
  pha
  lda ($3e),y
  sec
  sbc #$3c
  asl a
  tax

  lda $8000,x
  sta $0112
  pla
  tax
  lda $0112
  jmp $eaec

 secondtime:
  dec $0110
  dec $3e
  lda $3e
  cmp #$ff
  bne skip

  dec $3f

 skip:
  txa
  pha
  ldy #$00
  lda ($3e),y 
  sec
  sbc #$3c
  asl a
  tax
  lda $8001,x
  
  sta $0112
  pla
  txa
  lda $0112
  jmp $eaec

aaaand it didn't work. to debug it i'll set a breakpoint on $88c3. wait that didn't work so i'll just manually step through each instruction

bavi over at the rhdn discord pointed out that I skipped a `bpl abort`. sounds like i'll have to reinsert the code

I gotta fix my code: https://discord.com/channels/266412086291070988/267170507705679872/1235375706628952074

And also use breno's insertion tool :p

02/05/2024

Currently working on new ways to integrate crowdin on our workflow. I made some edits to Breno's tool using chatgpt (which is fully working!!!).

03/05/2024

I decided that it might not be a great idea to work on the crowdin workflow atm. It might be better if I just focus on getting *something* working (as abridgewater told me to do), such as DTE.

I made a thread for the project on RHDN. I gave up trying to support DTE from Kingmike's doc and instead am using this routine given to me by jroweboy:

exit = $eaec

  beq $f7dd ; Line added by me to fix his code
entrypoint:
  ; Load the first/second flag into the carry
  lsr $0110
  ; Load the next character
  ldy #$00
  lda ($3e),y
  ; If the second flag is set branch
  bcs secondtime
  ; Check that the character is in the range [$3b, $6d)
  ; TODO: Why is it this range?
  cmp #$3b
  bcc abort
  cmp #$6d
  bcs abort

  ; Set the first/second toggle to second
  inc $0110

  ; a is the new character, so subtract $3c to get the correct offset
  ; sec - carry is clear so we can skip sec by subtracting 1 to the value to subtract
  sbc #$3c - 1
  asl a
  tay

  lda $8000,y
abort:
  jmp exit

secondtime:
  ; first/second toggle is cleared already so don't need to set it

  ; decrement the pointer
  ldy $3e
  bne skip
    dec $3f
skip:
  dec $3e
  
  ; a is the new character, so subtract $3c to get the correct offset
  ; sec - carry is already set from the `lsr $110` so we don't need to set it
  sbc #$3c
  asl a
  tay
  lda $8001,y
  jmp exit

wooooo!! i got an early version of it working. But some dte combinations are still buggy... I need to investigate this. qwp

ok, i found out what the bug is. there are two bugs here:

    $0110 isn't always 00
    $8000 has no dte table and seems to be used for other types of data

this is merely a matter of swapping out $0110 for another mem address and inserting a dte table on another address. :3
i think all i need to do now is put @jroweboy 's code on an assembler and make the rest of the changes from there.
something like cc65, i mean

Speaking of assemblers, I still gotta get a version of denim's bass assembler. qwp

actually nvm, i'll stick to xkas. links:

https://github.com/oneup40/xkas-v6-linux
https://discord.com/channels/266412086291070988/267170507705679872/1236141274541719634

04/05/2024

I'm gonna learn to use xkas assembler today :o

here's an example of code:

```
MyFunction: {
  LDA #$00 : STA $00
}
```

i also gotta do a main.asm file like this:

```
org $008000 ; set writing position to $008000
incsrc "myfile1.asm"
incsrc "myfile2.asm"

incbin "myfile.bin"
```

ok i got the code to assemble but i also gotta figure out why it's glitching out qwp

turns out xkas-v6 on linux doesn't compute org instructions :(

here's my ancient code though:

```
org $000

print org

exit      = $eaec ; cpu address where the original routine left off. euivalent to 0x7eafc in rom
freeram   = $0110 ; free address in ram 
dtetbl    = $8000 ; cpu address where the dte table is stored
freespace = $f84a ; space to place the dte routine in
txt_ptr   = $3e   ; pointer to current tile in memory
txt_ptr2  = $3f

org $7eaf6 {    ; location where the original text loading routine is. euivalent to $eae6 in cpu
  ldy #$00      ; part of the original ff2's routine
  jmp freespace ; move to free space
}

print org

org $7f85a        ; equivalent to $f84a in ram

entrypoint: {
  beq terminator  ; part of the original routine
  lsr $0110       ; Load the first/second flag into the carry
  lda (txt_ptr),y ; Load the next character 
  bcs secondtime  ; If the second flag is set branch  
  cmp #$3b
  bcc abort
  cmp #$6d        ; Check that the character is in the range [0x3b-0x6d)
  bcs abort
}

{
  inc freeram ; Set the first/second toggle to second

  sbc #$3c-1  ; a is the new character, so subtract $3c to get the correct offset
; sec - carry is clear so we can skip sec by subtracting 1 to the value to subtract
  asl a
  tay

  lda $8000,y  
}
  
abort: {
  jmp exit
}

secondtime: {
  ldy txt_ptr  ; first/second toggle is cleared already so don't need to set it
  bne skip
}
  dec txt_ptr2 ; decrement the pointer
  
skip: {
  dec txt_ptr
  sbc #$3c        ; a is the new character, so subtract $3c to get the correct offset
; sec - carry is already set from the `lsr $110` so we don't need to set it
  asl a
  tay
  lda dtetbl+1,y
  jmp exit
}

terminator: { ; handles end string
  jmp $f7dd
}
```

I gotta fix my own asm code by referencing tracy.asm: https://discord.com/channels/266412086291070988/267170507705679872/1236132506378571857

The discussion goes on... https://discord.com/channels/266412086291070988/267170507705679872/1236507573700792402

05/05/2024

phonymike is helping me convert my code from xkas v6 to xkas-plus v14+1.

here's my code again:

```
//;xkas-plus v14+1
arch 6502
header
banksize $4000

org $00

print org

exit      = $eaec //; cpu address where the original routine left off. euivalent to 0x7eafc in rom
freeram   = $0110 //; free address in ram 
dtetbl    = $8000 //; cpu address where the dte table is stored
freespace = $f84a //; space to place the dte routine in
txt_ptr   = $3e   //; pointer to current tile in memory
txt_ptr2  = $3f

org $7eaf6 {    //; location where the original text loading routine is. euivalent to $eae6 in cpu
  ldy #$00      //; part of the original ff2's routine
  jmp freespace //; move to free space
}

print org

org $7f85a        //; equivalent to $f84a in ram

entrypoint: {
  beq terminator  //; part of the original routine
  lsr $0110       //; Load the first/second flag into the carry
  lda (txt_ptr),y //; Load the next character 
  bcs secondtime  //; If the second flag is set branch  
  cmp #$3b
  bcc abort
  cmp #$6d        //; Check that the character is in the range [0x3b-0x6d)
  bcs abort
}

{
  inc freeram //; Set the first/second toggle to second

  sbc #$3c-1  //; a is the new character, so subtract $3c to get the correct offset
//; sec - carry is clear so we can skip sec by subtracting 1 to the value to subtract
  asl a
  tay

  lda $8000,y  
}
  
abort: {
  jmp exit
}

secondtime: {
  ldy txt_ptr  //; first/second toggle is cleared already so don't need to set it
  bne skip
}
  dec txt_ptr2 //; decrement the pointer
  
skip: {
  dec txt_ptr
  sbc #$3c        //; a is the new character, so subtract $3c to get the correct offset
//; sec - carry is already set from the `lsr $110` so we don't need to set it
  asl a
  tay
  lda dtetbl+1,y
  jmp exit
}

terminator: { //; handles end string
  jmp $f7dd
}
```

the reason my most recent code doesn't work is because of the .org offsets, and i need to learn nes assembly magic for it. here's a ref: https://discord.com/channels/266412086291070988/267170507705679872/1236785038238941194

and here's the latest code:


```
// xkas-plus v14+1
arch 6502
header
banksize $4000

org $00

print "current pc: ",org

define exit      $eaec // cpu address where the original routine left off. euivalent to 0x7eafc in rom
define freeram   $0110 // free address in ram 
define dtetbl    $8000 // cpu address where the dte table is stored
define freespace $f84a // space to place the dte routine in
define txt_ptr   $3e   // pointer to current tile in memory
define txt_ptr2  $3f

org $7eaf6        // location where the original text loading routine is. euivalent to $eae6 in cpu
  ldy #$00        // part of the original ff2's routine
  jmp {freespace} // move pc to free space


org $7f85a        // equivalent to $f84a in ram

print "current pc: ",org

entrypoint: 
  beq terminator  // part of the original routine
  lsr $0110       // Load the first/second flag into the carry
  lda ({txt_ptr}),y // Load the next character 
  bcs secondtime  // If the second flag is set branch  
  cmp #$3b
  bcc abort
  cmp #$6d        // Check that the character is in the range [0x3b-0x6d)
  bcs abort



  inc {freeram}  // Set the first/second toggle to second

   sbc #($3c-1)  // a is the new character, so subtract $3c to get the correct offset
//sec - carry is clear so we can skip sec by subtracting 1 to the value to subtract
  asl
  tay

  lda $8000,y  

  
abort: 
  jmp {exit}


secondtime: 
  ldy {txt_ptr}  // first/second toggle is cleared already so don't need to set it
  bne skip

  dec {txt_ptr2} // decrement the pointer
  
skip: 
  dec {txt_ptr}
  sbc #$3c       // a is the new character, so subtract $3c to get the correct offset
//sec - carry is already set from the `lsr $110` so we don't need to set it
  asl
  tay
  lda {dtetbl}+1,y
  jmp {exit}


terminator:     // handles end string
  jmp $f7dd
```

05/05/2024

hey there. i'm writing this at 2 am. hopefully i'll get ungrounded by my parents qwp

i also might as well as for help on the fórum unificado. I might be able to log in with my lexicon email

I also added this to my checklist:

- Look into a way of integrating Crowdin into your workflow by making a help post in https://community.crowdin.com/ this should help :O

namely i want to not only assemble stuff on crowdin through command line but also compare existing translations into non-portuguese languages

07/05/2024

glacie took a look at the crowdin api but she doesn't know how to operate it qwp i should link to her on the CLI once she's better

Breno's working on making translation memory files out of the Castlefynn script dumps I'm lifting from the rest of the FF2 translations. :D

08/05/2024

I think i'm going to take a break qwp

So far all I managed to do was extract Toma's script with castlefynn. When I'm able to i'll dump all of the castlefynn script files on a folder

I also asked on the brazilian FFXIV discord for help on localizing the game. I gotta git gud at translating qwp

09/05/2024

June is working on a python script to convert Castlefynn scripts into Atlas scripts and vice versa. What I can do until then is extract text from other translations of FF2 to use on my crowdin comparisons (which I did. I extracted Toma's, Demiforce's, Chaos Rush's and the Prototype's)

10/05/2024

Not sure what to do lmao.

I just had a discussion on rhdn pertaining to the legality of this project.

I decided to do the following:
- post the CODE to my translation on github, but license it under gpl;
- post the scripts (translated and otherwise) on crowdin and not license it or anything;

I should work on making a github repo.

https://discord.com/channels/266412086291070988/579808915340918786/1238541527609901158

13/05/2024

Hey, it's been a while since I last worked on this.

I dumped the text from CBT's brpt ff2 translation and I THINK kavika will help me make the github repo. Hopefully dgshoe will help me proofread my former script dumps uwu

Anyway i've been too busy atm arguing with my family so ig i'll leave bye

Ps: I've been spending too much time today trying to get abcde to extract text for me. qwp

14/05/2024

Still working on extracting text from other ff2 translations. I'm working on dumping the ones from Minh's french translation of ff2, but I'm having trouble with it solely because I can't find the DTE table. But I managed to find the upper one at $3eef3 in the Demiforce rom, and have yet to find the lower. Turns out it stores the upper and lower letters separately xd

15/05/2024

ok I found the second half for it in the demiforce rom.

DTE upper = $3eef3
DTE lower = $3ef26

With this I was able to make the tables for various FF2 translations and extract the text from them! Woohoo!

Now all that remains is making the table for russian ff2 and extracting its text. OwO

17/05/2024

I managed to get a clean dump with abcde. I don't have much to add on this entry because I'm writing this entry *after* I already did the work.

18/05/2024

Not much to add here. I'm considering switching over from github and crowdin to gitlab and weblate respectively simply bc they're free as in freedom.

19/05/2024

I made some adjustments to dump.sh, and it should be partially working now. All that's left to do on that end is:

- write the commands for the Atlas to JSON converter Breno made
- fix the Atlas to JSON converter

The main issue seems to be in Breno's converter. It doesn't work with code like this:

```
This is a linebreak<LINE>
and this is an end<END>
```

Additionally it expects each string to begin with "//POINTER" and "#W16".


Using Chatgpt's GPT-4 AI i managed to fix it, but it still expects a //POINTER at the start as well as doesn't keep trailing spaces before <END> tags.

It's highly unlikely that I'll find someone to do this for me, so I might as well just learn python and fix this on my own.

matsu from jacutem fixed the //pointer requirement, but I still have to:

- fix the trailing spaces before <END> and 
- dump the russian rom's text
- extract text from the unknown english translation

21/05/2024

Gitlab repository has been created.

22/05/2024

Currently waiting for abw and devinacker to give me explicit permission to redistribute and remix their abcde and xkas-plus programs, respectively.

23/05/2024

devinacker replied and says xkas-plus is public domain. I made a separate gitea repo for the text.

24/05/2024

Changelog file created.

BTW this journal has quickly degenerated into a simple changelog, hasn't it? >:p

discussion on the gitea repo here:

https://discord.com/channels/266412086291070988/579808915340918786/1243553654003339306
